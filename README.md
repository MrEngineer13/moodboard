# MoodBoard
Uses Expert.ai to generate sentiment score from typed sentences.

## Inspiration 
Saw paper about using keystrokes to detect alzheimers.

## What it does
Listens in background for sentence termination. Gets positive & negative sentiment from API. Then it saves daily high and low for 50 day running average for positive & negative sentiment. Then these sentiment data are plotted on a graph to see trends.

## How we built it
Started with python to watch for keystrokes in background, then saved to db and plotted with Matplot lib.

## Challenges we ran into
Was new to keyboard library so learned it had function to convert key sequences to words and lacks the ability to listen for multiple sentence termination keys, .,?,!.

## Accomplishments that we're proud of
Every day at midnight calculating moving 50 day sentiments.

## What we learned
Mostly about calculating moving averages and plotting them.

## What's next for MoodBoard
Breaking  daily average down to hourly, listening to all sentence terminations and sharing sentiment scores with others.
