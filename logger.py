import os, sys, termios
# Using Keyboard module in Python
import keyboard
import os
import pdb
import shelve
import datetime
import timeit
import random
import matplotlib.pyplot as plt

from expertai.nlapi.cloud.client import ExpertAiClient


def today():
    return datetime.date.today()


def plot(values):
    print("plot")
    print(values)
    x = [0]*len(values)
    print(len(values))
    for i in range(len(values)):
        x[i] = i
    print(x)
    print(values)
    plt.plot(values, x)
    plt.ylabel('Days Ago')
    plt.xlabel('Max positive sentiment')
    plt.show()


def keyboardKey(k):
    if k == '\x1b':  # esc
        return "esc"
    elif k == '\x7f':  # backspace
        return "backspace"
    elif k == '\n':  # entera
        return "enter"
    elif k == '\x20':  # space
        return "space"
    elif k == '\x1b[A':  # arrow up
        return "up"
    elif k == '\x1b[B':  # arrow down
        return "down"
    elif k == '\x1b[C':  # arrow right
        return "right"
    elif k == '\x1b[D':  # arrow left
        return "left"
    else:
        return (k)


def time_until_end_of_day(dt=None):
    # type: (datetime.datetime) -> datetime.timedelta
    """
    Get timedelta until end of day on the datetime passed, or current time.
    """
    if dt is None:
        dt = datetime.datetime.now()
    tomorrow = dt + datetime.timedelta(days=1)
    return datetime.datetime.combine(tomorrow, datetime.time.min) - dt


def main():
    d = shelve.open('daily_avg.txt')  # here you will save the score variable


    client = ExpertAiClient()
    language = 'en'

    # while True:
    #     key = keyboardKey()
    #     if key == 'esc':
    #         quit()
    #     else:
    #         print(key)

    # It writes the content to output Great.
    # keyboard.write("GEEKS FOR GEEKS\n")

    # It writes the keys r, k and endofline
    # keyboard.add_hotkey('a', lambda: keyboard.write("Jon\n",0))
    # keyboard.press_and_release('shift + r, shift + k, \n')
    # keyboard.press_and_release('R, K')
    max_pos = 0
    max_neg = 0
    while True:
        # It records all the keys until escape is pressed the world is great. testing. testing. . The world is great.
        # Today is a good day. great.

        # rk = keyboard.record(until='b')
        text = 'Today is a good day. I love to go to mountain.'

        keyboard.add_hotkey('p', lambda: plot(d['pos_amount']))
        keyboard.add_hotkey('n', lambda: plot(d['neg_amount']))

        t = keyboard.get_typed_strings(keyboard.record(until='.'), allow_backspace=True)
        temp = ""
        for s in t:
            print(s)
            temp += s

        document = client.specific_resource_analysis(
            body={"document": {"text": temp}},
            params={'language': language, 'resource': 'sentiment'})
        print(temp, "sentiment:", document.sentiment.positivity)
        print(temp, "sentiment:", document.sentiment.negativity)
        print(temp, "sentiment:", document.sentiment.overall)
        pos = document.sentiment.positivity
        neg = document.sentiment.negativity
        # max_pos = max(max_pos, pos)
        # max_neg = min(max_neg, neg)
        max_pos = 1
        max_neg = -1
        print(temp)

        if time_until_end_of_day().seconds == 0 or True:
            midnight(max_pos, max_neg)

        # It replay back the all keys
        # will this
        # keyboard.play(rk, speed_factor=1) tes
    # it blocks until ctrl is pressed
    # keyboard.wait('Ctrl') test


# https://docs.python.org/release/2.3.5/lib/itertools-example.html
# >>> for checknum, amount in izip(count(1200), amounts):
# ...     print 'Check %d is for $%.2f' % (checknum, amount)

def midnight(max_pos, max_neg):
    global n
    d = shelve.open('daily.txt')  # here you will save the score variable

    print(max_pos)
    print(max_neg)
    if 'day_top_' + today().isoformat() not in d:
        d['day_top_' + today().isoformat()] = [max_pos, max_neg]  # thats all, now it is saved on disk.
    print()
    print(today().isoformat())
    print('pos_amount' in d and 'neg_amount' in d)
    if 'pos_amount' in d and 'neg_amount' in d:
        print(d['pos_amount'])
        print(d['neg_amount'])
        pos_slice = d['pos_amount']
        neg_slice = d['neg_amount']
        # pos_slice.append(max_pos)
        # neg_slice.append(max_neg)
        print(pos_slice)
        print(neg_slice)
    else:
        pos_slice, neg_slice = [], [0] * 51
        for i in range(0, 5):
            n = random.randint(1, 30)
        pos_slice.append(n)
        pos_slice.append(max_pos)
        neg_slice.append(max_neg)
        print(pos_slice)
        print(neg_slice)
    print(pos_slice)
    print(neg_slice)
    d['pos_amount'] = pos_slice[1:51]
    d['neg_amount'] = neg_slice[1:51]
    print(d['pos_amount'])
    plot(pos_slice)

    d.close()


if __name__ == '__main__':
    main()
# class keyboard:
#     def __call__(self):
#         fd = sys.stdin.fileno()
#         old = termios.tcgetattr(fd)
#         new = termios.tcgetattr(fd)
#         new[3] = new[3] & ~termios.ICANON & ~termios.ECHO
#         new[6][termios.VMIN] = 1
#         new[6][termios.VTIME] = 0
#         termios.tcsetattr(fd, termios.TCSANOW, new)
#         c = None
#         try:
#             c = os.read(fd, 3)
#         finally:
#             termios.tcsetattr(fd, termios.TCSAFLUSH, old)
#         return c
